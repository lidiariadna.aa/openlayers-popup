# OpenLayers-popup

Este ejemplo muestra el proceso para integrar ventanas emergentes(_popup_) en un mapa, las cuales despliegan las coordenadas del punto donde el usuario hace click. En este caso se emplea como mapa base: **_[Maptilers](https://www.maptiler.com/)_**.

Comenzamos por importar las clases necesarias. La clase `ol/Overlay` es la que nos proporcionara un elemento que se mostrará sobre el mapa y se adjuntará a una única ubicación del mapa.  Se puede personalizar especificando su posición exacta en coordenadas del mapa, controlar su despliegue (por ejemplo, mostrándolo u ocultándolo en respuesta a eventos del usuario), y personalizar su contenido HTML para mostrar texto, imágenes, o incluso controles interactivos. Tambien en el archivo CSS podemos personalizar el tamaño, color y/o texto de la ventana.

Luego, se definen los elementos HTML que compondrán el popup (en este caso _popup_, _popup-content_, _popup-closer_)

Para definir el popup utilizando la clase _ol/Overlay.js_ de OL es de la siguiente forma:

```js
const overlay = new Overlay({
  element: container,
  autoPan: {
    animation: {
      duration: 100,
    },
  },
});
```

Con la clase `ol.layer.Tile` se cargan y muestran los datos de mapas del proyecto _OpenStreetMap_, y con la *_API_* de *_Maptiler CLOUD_* podemos acceder a mapas prediseñados o personalizar propios, lo cual requiere que previamente hayamos obtenido una clave personal al registrarnos en *_Maptiler_*. En este caso se uso _streets_ como mapa base https://cloud.maptiler.com/maps/streets-v2/. Y con `ol.souce.XYZ` se especificará la fuente de los datos.

```js
const map = new Map({
  layers: [
    new TileLayer({
      source: new XYZ({
        attributions: attributions,
        url: 'https://api.maptiler.com/maps/streets-v2/{z}/{x}/{y}.png?key=' + key,
        tileSize: 512,
      }),
    }),
  ],
```

Para finalizar se configurará el controlador de eventos de clic en el mapa, que activará y mostrará la ventana emergente con las coordenadas seleccionadas.

```js
map.on('singleclick', function (evt) {
  const coordinate = evt.coordinate;
  const hdms = toStringHDMS(toLonLat(coordinate));

  content.innerHTML = '<p>Hiciste click aquí:</p><code>' + hdms + '</code>';
  overlay.setPosition(coordinate);
});
```

