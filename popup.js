import './style.css';
import {Map, View} from 'ol';
import Overlay from 'ol/Overlay.js';
import TileLayer from 'ol/layer/Tile.js';
import XYZ from 'ol/source/XYZ.js';
import {toLonLat} from 'ol/proj.js';
import {toStringHDMS} from 'ol/coordinate.js';


/*Elements that make up the popup.*/
const container = document.getElementById('popup');
const content = document.getElementById('popup-content');
const closer = document.getElementById('popup-closer');

/* Create an overlay to anchor the popup to the map.*/
const overlay = new Overlay({
  element: container,
  autoPan: {
    animation: {
      duration: 100,
    },
  },
});

/*Add a click handler to hide the popup. 
 * @return {boolean} Don't follow the href.*/
closer.onclick = function () {
  overlay.setPosition(undefined);
  closer.blur();
  return false;
};

const key = '5yEBnZHcryRSLiHOQCau';
const attributions =
  '<a href="https://www.maptiler.com/copyright/" target="_blank">&copy; MapTiler</a> ' +
  '<a href="https://www.openstreetmap.org/copyright" target="_blank">&copy; OpenStreetMap contributors</a>';

/*Create the map.*/
const map = new Map({
  layers: [
    new TileLayer({
      source: new XYZ({
        attributions: attributions,
        url: 'https://api.maptiler.com/maps/streets-v2/{z}/{x}/{y}.png?key=' + key,
        tileSize: 512,
      }),
    }),
  ],
  overlays: [overlay],
  target: 'map',
  view: new View({
    center: [0, 0],
    zoom: 2,
  }),
});

/* Add a click handler to the map to render the popup. */
map.on('singleclick', function (evt) {
  const coordinate = evt.coordinate;
  const hdms = toStringHDMS(toLonLat(coordinate));

  content.innerHTML = '<p>Hiciste click aquí:</p><code>' + hdms + '</code>';
  overlay.setPosition(coordinate);
});